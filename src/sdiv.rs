use number_traits::Num;

use super::zero;

#[inline]
pub fn sdiv<'a, T: Copy + Num>(out: &'a mut [T; 4], a: &[T; 4], s: T) -> &'a mut [T; 4] {
    if s == T::zero() {
        zero(out)
    } else {
        out[0] = a[0] / s;
        out[1] = a[1] / s;
        out[2] = a[2] / s;
        out[3] = a[3] / s;
        out
    }
}
#[test]
fn test_sdiv() {
    let mut v = [0, 0, 0, 0];
    sdiv(&mut v, &[1, 1, 1, 1], 1);
    assert!(v[0] == 1);
    assert!(v[1] == 1);
    assert!(v[2] == 1);
    assert!(v[3] == 1);
}
